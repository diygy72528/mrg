package com.mrg.frame.mrg.controller;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @ClassName IndexController
 * @Description TODO
 * @Author guyao
 * @Date 2019/5/23 0023 下午 4:12
 * @Version 1.0
 */
@Controller
@RequestMapping
public class IndexController {

    @RequestMapping("/index/user")
    public String index() {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return "index";
    }

    @GetMapping("/login")
    public String login() {
        return "login";
    }



    @GetMapping("/abort")
    @ResponseBody
    public String abort() {
        return "abort";
    }
}
