package com.mrg.frame.mrg.dao;

import com.mrg.frame.mrg.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @ClassName UserDao
 * @Description TODO
 * @Author guyao
 * @Date 2019/5/23 0023 下午 3:40
 * @Version 1.0
 */
public interface UserDao extends JpaRepository<User, String> {
    User findByUserName(String username);
}
