package com.mrg.frame.mrg.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.List;

/**
 * @ClassName Role
 * @Description TODO
 * @Author guyao
 * @Date 2019/5/23 0023 下午 1:21
 * @Version 1.0
 */
@Entity
@Table(name = "role")
public class Role extends BaseModel {

    @Column(name = "role_name")
    private String roleName;

    @ManyToMany(mappedBy = "roleList")
    private List<User> userList;

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }
}
