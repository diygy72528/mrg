package com.mrg.frame.mrg.model;

import javax.persistence.*;
import java.util.List;

/**
 * @ClassName User
 * @Description TODO
 * @Author guyao
 * @Date 2019/5/23 0023 下午 1:21
 * @Version 1.0
 */
@Entity
@Table(name = "user")
public class User extends BaseModel {

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_role",
            joinColumns = { @JoinColumn(name = "userId") },
            inverseJoinColumns = { @JoinColumn(name = "roleId") }
    )
    private List<Role> roleList;

    @Column(name = "user_name")
    private String userName;

    @Column(name = "password")
    private String password;

    public List<Role> getRoleList() {
        return roleList;
    }

    public void setRoleList(List<Role> roleList) {
        this.roleList = roleList;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
