package com.mrg.frame.mrg.service;

import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * @ClassName UserService
 * @Description TODO
 * @Author guyao
 * @Date 2019/5/23 0023 下午 3:41
 * @Version 1.0
 */
public interface UserService extends UserDetailsService {
}
