package com.mrg.frame.mrg.service.impl;

import com.mrg.frame.mrg.dao.UserDao;
import com.mrg.frame.mrg.model.Role;
import com.mrg.frame.mrg.model.User;
import com.mrg.frame.mrg.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName UserServiceImpl
 * @Description TODO
 * @Author guyao
 * @Date 2019/5/23 0023 下午 3:42
 * @Version 1.0
 */

@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userDao.findByUserName(username);
        if(user == null) {
            throw new UsernameNotFoundException("未找到用户");
        }
        ArrayList<SimpleGrantedAuthority> authorities = new ArrayList<>();
        List<Role> roleList = user.getRoleList();
        for (Role role : roleList) {
            //SimpleGrantedAuthority au = new SimpleGrantedAuthority(role.getRoleName());
            //authorities.add(au);
        }
        return new org.springframework.security.core.userdetails.User(user.getUserName(),user.getPassword(),authorities);
    }
}
