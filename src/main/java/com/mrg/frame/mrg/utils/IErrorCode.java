package com.mrg.frame.mrg.utils;

public interface IErrorCode {

    long getCode();

    String getMessage();

}
